# Private Modules Demo

[![Coverage Status](https://coveralls.io/repos/bitbucket/benjamincoe/private-modules-demo/badge.svg?branch=master)](https://coveralls.io/bitbucket/benjamincoe/private-modules-demo?branch=master)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg)](https://github.com/conventional-changelog/standard-version)

Demonstrates using private npm modules with Bitbucket's Pipelines.

## Private Modules on Public Registry

1. use the `bitbucket-pipelines.yml` supplied in [this repository](https://bitbucket.org/benjamincoe/private-modules-demo/src).
2. set the `NPM_TOKEN` environment variable:
  * this token can be found in your local `~/.npmrc`, after you login to the registry.
3. enable pipelines, and you're ready to go.

## Private Modules in npm Enterprise

1. use the `bitbucket-pipelines.yml` supplied in [this repository](https://bitbucket.org/benjamincoe/private-modules-demo/src).
2. set the `NPM_TOKEN` environment variable:
  * this token can be found in your local `~/.npmrc`, after you login to the registry.
3. set `NPM_REGISTRY_URL` to the full URL of your private registry (with scheme).
4. enable pipelines, and you're ready to go.

## License

ISC
